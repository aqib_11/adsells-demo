import cv2
import numpy as np
import numpy
import time
from awss3 import *

import json
from microsoftfaceapi3 import *
from pytz import timezone
from contextlib import contextmanager
from wide_resnet import WideResNet
from keras.utils.data_utils import get_file
import traceback
import ast

import os
from multiprocessing import Queue
import re
import sys
from deepgaze.face_detection import HaarFaceDetector
from deepgaze.face_landmark_detection import faceLandmarkDetection
import math
import csv
import imutils
import multiprocessing

from threading import Thread
import json
from datetime import datetime
from collections import Counter
#from imutils.video import WebcamVideoStream
import boto3
from boto3.dynamodb.conditions import Key, Attr
from app_utils import *
from multiprocessing import Pool, Queue, Process, Value

from ctypes import *
import pandas as pd
import dlib
from imutils.video import VideoStream
from imutils import face_utils

manager = multiprocessing.Manager()



display = manager.list()



#If True enables the verbose mode
DEBUG = False



P3D_RIGHT_SIDE = numpy.float32([-100.0, -77.5, -5.0]) #0
P3D_GONION_RIGHT = numpy.float32([-110.0, -77.5, -85.0]) #4
P3D_MENTON = numpy.float32([0.0, 0.0, -122.7]) #8
P3D_GONION_LEFT = numpy.float32([-110.0, 77.5, -85.0]) #12
P3D_LEFT_SIDE = numpy.float32([-100.0, 77.5, -5.0]) #16
P3D_FRONTAL_BREADTH_RIGHT = numpy.float32([-20.0, -56.1, 10.0]) #17
P3D_FRONTAL_BREADTH_LEFT = numpy.float32([-20.0, 56.1, 10.0]) #26
P3D_SELLION = numpy.float32([0.0, 0.0, 0.0]) #27
P3D_NOSE = numpy.float32([21.1, 0.0, -48.0]) #30
P3D_SUB_NOSE = numpy.float32([5.0, 0.0, -52.0]) #33
P3D_RIGHT_EYE = numpy.float32([-20.0, -65.5,-5.0]) #36
P3D_RIGHT_TEAR = numpy.float32([-10.0, -40.5,-5.0]) #39
P3D_LEFT_TEAR = numpy.float32([-10.0, 40.5,-5.0]) #42
P3D_LEFT_EYE = numpy.float32([-20.0, 65.5,-5.0]) #45
#P3D_LIP_RIGHT = numpy.float32([-20.0, 65.5,-5.0]) #48
#P3D_LIP_LEFT = numpy.float32([-20.0, 65.5,-5.0]) #54
P3D_STOMION = numpy.float32([10.0, 0.0, -75.0]) #62

#The points to track
#These points are the ones used by PnP
# to estimate the 3D pose of the face
TRACKED_POINTS = (0, 4, 8, 12, 16, 17, 26, 27, 30, 33, 36, 39, 42, 45, 62)
ALL_POINTS = list(range(0,68)) #Used for debug only





people_data = manager.list()
data = manager.list()
face_ids = manager.list()
face_ids2 = manager.list()

emotion_images = manager.list()

lock = multiprocessing.Lock()


def start_stream(input_q):
    global lock
    usingPiCamera = True
    #vs = manager.list()
    frameSize = (640, 480)
    vs = VideoStream(src=0, usePiCamera=usingPiCamera, resolution=frameSize,
		    framerate=10).start()
    time.sleep(0.1)
    global lock
    while 1:
        frame = vs.read()
        if frame is not None:
            #lock.acquire()
            #if input_q1.qsize() <= 5:
                #input_q1.put(frame)
            #if input_q1.qsize() > 40:
                #lock.acquire()
                #while input_q1.qsize() != 20:
                #    input_q1.get()
                #lock.release()
           
            #input_q1.put(frame)
           
            if input_q.qsize() < 20:
                input_q.put(frame)
            else:
                lock.acquire()
                while input_q.qsize() != 0:
                    input_q.get()
                lock.release()
                input_q.put(frame)
            #lock.release()


def draw_label(image, point, label, font=cv2.FONT_HERSHEY_SIMPLEX,
               font_scale=1, thickness=2):
    size = cv2.getTextSize(label, font, font_scale, thickness)[0]
    x, y = point
    cv2.rectangle(image, (x, y - size[1]), (x + size[0], y), (255, 0, 0), cv2.FILLED)
    cv2.putText(image, label, point, font, font_scale, (255, 255, 255), thickness)

def cropFace(faces,frame,name):
    
    image = frame[faces[1] -20:faces[3]+20, faces[0] -20 :faces[2]+20]
    image = imutils.resize(image, width=400)
    cv2.imwrite(name,image)
    return name

def insert_item(table_name, item):
    session = boto3.session.Session()
    dynamodb = session.resource('dynamodb', region_name='us-east-2')
    table = dynamodb.Table(table_name)
    response = table.put_item(Item=item)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return True
    else:
        return False

def insert_dynamo_count(count):

    tableName = 'Clinic-counter'


    #for i,u in enumerate(url):
    
    face_data = {"count":count,"device":"rpi2"}
    #final_emotion.append(emotion_data)
    #final_emotion = dict(final_emotion, **emotion_data)

    #face_parameters = {"ad_url": url,"ad_id":ad_id}
#engagement_parameters = dict(engagement_parameters, **final_emotion)


    required_hash_data = {
        "key": "count",
        "timestamp": str(datetime.now(timezone('Asia/Karachi')))
        }
    item = dict(required_hash_data, **face_data)
    insert = insert_item(tableName,item)
    if insert:
        print(str(datetime.now())+" Count inserted")
        print(insert)
    #return insert

def insert_dynamo_count2(count):

    tableName = 'Clinic-counter2'


    #for i,u in enumerate(url):
    
    face_data = {"count":count,"device":"rpi2"}
    #final_emotion.append(emotion_data)
    #final_emotion = dict(final_emotion, **emotion_data)

    #face_parameters = {"ad_url": url,"ad_id":ad_id}
#engagement_parameters = dict(engagement_parameters, **final_emotion)


    required_hash_data = {
        "key": "count",
        "timestamp": str(datetime.now(timezone('Asia/Karachi')))
        }
    item = dict(required_hash_data, **face_data)
    insert = insert_item(tableName,item)
    if insert:
        print(str(datetime.now())+" Count inserted")
        print(insert)
    #return insert



def insert_dynamo(url,faces,gender,age):

    tableName = 'Counting-demographics-2'


    #for i,u in enumerate(url):
    
    face_data = {"url":url,"age":int(round(float(age))),"faces_data":str(faces), "device": "webcam"}
    #final_emotion.append(emotion_data)
    #final_emotion = dict(final_emotion, **emotion_data)

    #face_parameters = {"ad_url": url,"ad_id":ad_id}
#engagement_parameters = dict(engagement_parameters, **final_emotion)


    required_hash_data = {
        "gender": gender,
        "timestamp": str(datetime.now(timezone('Asia/Karachi')))
        }
    item = dict(required_hash_data, **face_data)
    insert = insert_item(tableName,item)
    print(insert)
    #return insert
    
    
    
def insert_dynamo_impressions(impr,time):

    tableName = 'impression-time'


    #for i,u in enumerate(url):
    #for i in range(len(age)):
    face_data = {"impression_count":impr,"time":time,"device":"rpi2"}
    #final_emotion.append(emotion_data)
    #final_emotion = dict(final_emotion, **emotion_data)

    #face_parameters = {"ad_url": url,"ad_id":ad_id}
#engagement_parameters = dict(engagement_parameters, **final_emotion)


    required_hash_data = {
        "key": "impressions",
        "timestamp": str(datetime.now(timezone('Asia/Karachi')))
        }
    item = dict(required_hash_data, **face_data)
    insert = insert_item(tableName,item)
    print(str(datetime.now())+"impression inserted")
    print(insert)
    #return insert
    
def insert_dynamo_local(age,gender):

    tableName = 'Counting-demographics-local-2'


    #for i,u in enumerate(url):
    for i in range(len(age)):
        face_data = {"age":age[i],"device":"rpi2"}
        #final_emotion.append(emotion_data)
        #final_emotion = dict(final_emotion, **emotion_data)

        #face_parameters = {"ad_url": url,"ad_id":ad_id}
    #engagement_parameters = dict(engagement_parameters, **final_emotion)


        required_hash_data = {
            "gender": gender[i],
            "timestamp": str(datetime.now(timezone('Asia/Karachi')))
            }
        item = dict(required_hash_data, **face_data)
        insert = insert_item(tableName,item)
        print(str(datetime.now())+"local results inserted")
        print(insert)
        #return insert

def insert_dynamo2(url,faces,gender,age):

    tableName = 'Counting-demographics-1'


    #for i,u in enumerate(url):
    
    face_data = {"url":url,"age":int(round(float(age))),"faces_data":str(faces),"device":"rpi2"}
    #final_emotion.append(emotion_data)
    #final_emotion = dict(final_emotion, **emotion_data)

    #face_parameters = {"ad_url": url,"ad_id":ad_id}
#engagement_parameters = dict(engagement_parameters, **final_emotion)


    required_hash_data = {
        "gender": gender,
        "timestamp": str(datetime.now(timezone('Asia/Karachi')))
        }
    item = dict(required_hash_data, **face_data)
    insert = insert_item(tableName,item)
    print(insert)
    #return insert


def get_results(name):
    global face_ids
    for img_path in name:
        imagename = uploadToS3(img_path)
        name1 = 'https://s3.ap-south-1.amazonaws.com/face-api-temp-storage/'+imagename
        response1 = get_facial_features(name1)
        response = response1.decode("utf-8")
        if response:
            data = json.loads(str(response))
            data2 = data
            if data:
        
                print ("Response")
                
                print (response)
                
                gender = str(data[0]['faceAttributes']['gender'])
                age1 = str(data[0]['faceAttributes']['age'])
                beard = str(data[0]['faceAttributes']['facialHair']['beard'])
                 
                age1 = float(age1) - (10*(float(beard)) + 5)
                face_ids.append(data[0]['faceId'])
                                        
                if(len(face_ids) >= 2):
                    
                    if (is_same_person(face_ids[0], face_ids[1])):
                        face_ids.pop(0)
                        continue
                    else:
						
                        t = Process(target = insert_dynamo2, args = (name1,data2,gender,age1,))
                        t.start()
                        t.join()
                    face_ids.pop(0)
                else:
                    t = Process(target = insert_dynamo2, args = (name1,data2,gender,age1,))
                    t.start()
                    t.join()
        else:
            print('no data from API')
            
def get_results2(name):
    global face_ids2
    for img_path in name:
        imagename = uploadToS3(img_path)
        name1 = 'https://s3.ap-south-1.amazonaws.com/face-api-temp-storage/'+imagename
        response1 = get_facial_features(name1)
        response = response1.decode("utf-8")
        if response:
            data = json.loads(str(response))
            data2 = data
            if data:
        
                print ("Response")
                
                print (response)
                
                gender = str(data[0]['faceAttributes']['gender'])
                age1 = str(data[0]['faceAttributes']['age'])
                beard = str(data[0]['faceAttributes']['facialHair']['beard'])
                 
                age1 = float(age1) - (10*(float(beard)) + 5)
                face_ids2.append(data[0]['faceId'])
                                        
                if(len(face_ids2) >= 2):
                    
                    if (is_same_person(face_ids2[0], face_ids2[1])):
                        face_ids2.pop(0)
                        continue
                    else:
                        t = Thread(target = insert_dynamo, args = (name1,data2,gender,age1,))
                        t.start()
                        t.join()
                    face_ids2.pop(0)
                else:
                    t = Thread(target = insert_dynamo, args = (name1,data2,gender,age1,))
                    t.start()
                    t.join()
        else:
            print("No data from API")


def main_pi():
    
    #Defining the video capture object
    

    #Create the main window and move it
    #cv2.namedWindow('Video')
    #cv2.moveWindow('Video', 20, 20)

    #Obtaining the CAM dimension
   

    #Defining the camera matrix.
    #To have better result it is necessary to find the focal
    # lenght of the camera. fx/fy are the focal lengths (in pixels) 
    # and cx/cy are the optical centres. These values can be obtained 
    # roughly by approximation, for example in a 640x480 camera:
    # cx = 640/2 = 320
    # cy = 480/2 = 240
    # fx = fy = cx/tan(60/2 * pi / 180) = 554.26
    

    #print("Estimated camera matrix: \n" + str(camera_matrix) + "\n")

    #These are the camera matrix values estimated on my webcam with
    # the calibration code (see: src/calibration):
    #camera_matrix = numpy.float32([[602.10618226,          0.0, 320.27333589],
     #                              [         0.0, 603.55869786,  229.7537026], 
     #                              [         0.0,          0.0,          1.0] ])
    usingPiCamera = True
    #vs = manager.list()
    frameSize = (640, 480)
    vs = VideoStream(src="rtsp://192.168.1.4").start()
    time.sleep(0.1)
    cam_w = 640
    cam_h = 480
    c_x = cam_w / 2
    c_y = cam_h / 2
    f_x = c_x / numpy.tan(60/2 * numpy.pi / 180)
    f_y = f_x

    #Estimated camera matrix values.
    camera_matrix = numpy.float32([[f_x, 0.0, c_x],
                                   [0.0, f_y, c_y], 
                                   [0.0, 0.0, 1.0] ])

    print(str(datetime.now())+"Estimated camera matrix: \n" + str(camera_matrix) + "\n")
    #Distortion coefficients
    #camera_distortion = numpy.float32([0.0, 0.0, 0.0, 0.0, 0.0])

    #Distortion coefficients estimated by calibration
    camera_distortion = numpy.float32([ 0.06232237, -0.41559805,  0.00125389, -0.00402566,  0.04879263])


    #This matrix contains the 3D points of the
    # 11 landmarks we want to find. It has been
    # obtained from antrophometric measurement
    # on the human head.
    landmarks_3D = numpy.float32([P3D_RIGHT_SIDE,
                                  P3D_GONION_RIGHT,
                                  P3D_MENTON,
                                  P3D_GONION_LEFT,
                                  P3D_LEFT_SIDE,
                                  P3D_FRONTAL_BREADTH_RIGHT,
                                  P3D_FRONTAL_BREADTH_LEFT,
                                  P3D_SELLION,
                                  P3D_NOSE,
                                  P3D_SUB_NOSE,
                                  P3D_RIGHT_EYE,
                                  P3D_RIGHT_TEAR,
                                  P3D_LEFT_TEAR,
                                  P3D_LEFT_EYE,
                                  P3D_STOMION])

    #Declaring the two classifiers
    #my_cascade = haarCascade("../..//etc/xml/haarcascade_frontalface_alt.xml", "../../etc/xml/haarcascade_profileface.xml")
    hfd = HaarFaceDetector("./etc/xml/haarcascade_frontalface_alt.xml", "./etc/xml/haarcascade_profileface.xml")
    
    #TODO If missing, example file can be retrieved from http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2
    my_detector = faceLandmarkDetection('./etc/shape_predictor_68_face_landmarks.dat')
    #my_face_detector = dlib.get_frontal_face_detector()
    #Error counter definition
    no_face_counter = 0

    #Variables that identify the face
    #position in the main frame.
    face_x1 = 0
    face_y1 = 0
    face_x2 = 0
    face_y2 = 0
    face_w = 0
    face_h = 0
    depth = 16
    k = 8
    weight_file = './pretrained_models/weights.18-4.06.hdf5'
    img_size = 64
    model = WideResNet(img_size, depth=depth, k=k)()
    model.load_weights(weight_file)
    img_w,img_h = cam_w,cam_h
    #Variables that identify the ROI
    #position in the main frame.
    DEBUG=True
    tracker = dlib.correlation_tracker()
    trackingFace = 0
   
    face_n = list()
    count_person = list()
    yaw_r = range(-112,150)
    pitch_r= range(0,90)
    df = pd.DataFrame()
    impressions = 0
    previous_imp = 0
    impr = 0
    time_spent = []
    name1 = './emotion_images/'+"person_"+str(datetime.now())+".jpg"
    boxes = []
    previous_count = 0
    counter = 0
    previous_enc = []
    links_list = []
    box2 = []
    now  = datetime.now()
    ages_data = []
    gender_data = []
    impresions = dict()
    check = True
    timer = []
    global lock
    global now_2
    while(True):
        then = datetime.now()
        diff = then - now
        #video_capture = cv2.VideoCapture("rtsp://192.168.1.104")
        #frames = video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
        #print(frames)
        #video_capture.set(cv2.CAP_PROP_POS_FRAMES,frames-1)
        

        
            #count_f = count_f.put([f])
        
        #if inv == False:
            
        #    print(f)
            
        #Create the main window and move it
        #cv2.namedWindow('Video')
        #cv2.moveWindow('Video', 20, 20)
        # Capture frame-by-frame
        frame = vs.read()
        frame.setflags(write=True)
        if frame is not None:
            #lock.acquire()
            #frame = input_q.get()
            #lock.release()
            #frame = imutils.resize(frame, width=500)
            #frame = cv2.cvtColor(frame[roi_y1:roi_y2, roi_x1:roi_x2], cv2.COLOR_BGR2GRAY)

            #Looking for faces with cascade
            #The classifier moves over the ROI
            #starting from a minimum dimension and augmentig
            #slightly based on the scale factor parameter.
            #The scale factor for the frontal face is 1.10 (10%)
            #Scale factor: 1.15=15%,1.25=25% ...ecc
            #Higher scale factors means faster classification
            #but lower accuracy.
            #
            #Return code: 1=Frontal, 2=FrontRotLeft, 
            # 3=FrontRotRight, 4=ProfileLeft, 5=ProfileRight.
            #my_cascade.findFace(gray, True, True, True, True, 1.10, 1.10, 1.15, 1.15, 40, 40, rotationAngleCCW=30, rotationAngleCW=-30, lastFaceType=my_cascade.face_type)
            faces_array = hfd.returnMultipleFacesPosition(frame, runFrontal=True, runFrontalRotated=False, 
                    runLeft=False, runRight=False, 
                    frontalScaleFactor=1.2, rotatedFrontalScaleFactor=1.2, 
                    leftScaleFactor=1.15, rightScaleFactor=1.15, 
                    minSizeX=64, minSizeY=64, 
                    rotationAngleCCW=30, rotationAngleCW=-30)
            faces = np.empty((len(faces_array), img_size, img_size, 3))
            ages_data = []
            gender_data = []
            #print("Total Faces: " + str(len(faces_array)))
            for i,element in enumerate(faces_array):

                face_x1 = int(element[0])
                face_y1 = int(element[1])
                face_x2 = int(face_x1+element[2])
                face_y2 = int(face_y1+element[3])
                w = element[2]
                h = element[3]
                text_x1 = face_x1
                text_y1 = face_y1 - 3
                #roi_x1 = face_x1 - roi_resize_w
                face_n = [face_x1,face_y1,face_x2,face_y2]
                #links_list.append(cropFace(face_n,frame,name1))
                xw1 = max(int(face_x1 - 0.4 * w), 0)
                yw1 = max(int(face_y1 - 0.4 * h), 0)
                xw2 = min(int(face_x2 + 0.4 * w), img_w - 1)
                yw2 = min(int(face_y2 + 0.4 * h), img_h - 1)
                faces[i, :, :, :] = cv2.resize(frame[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))
                

                if DEBUG == True:
                    cv2.putText(frame, "FACE " + str(1), (text_x1,text_y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1);
                    cv2.rectangle(frame, 
                                 (face_x1, face_y1), 
                                 (face_x2, face_y2), 
                                 (0, 255, 0), 
                                  2)            

                landmarks_2D = my_detector.returnLandmarks(frame, face_x1, face_y1, face_x2, face_y2, points_to_return=TRACKED_POINTS)

                if DEBUG == True:
                    for point in landmarks_2D:
                        cv2.circle(frame,( point[0], point[1] ), 2, (0,0,255), -1)

                retval, rvec, tvec = cv2.solvePnP(landmarks_3D, 
                                                      landmarks_2D, 
                                                      camera_matrix, camera_distortion)

               
                axis = numpy.float32([[50,0,0], 
                                          [0,50,0], 
                                          [0,0,50]])
                
                R = cv2.Rodrigues(rvec)[0]
                            #rotation_value =  math.sqrt(abs(rotation_vector[0]*rotation_vector[0]) + abs(rotation_vector[1]*rotation_vector[0]) + abs(rotation_vector[2]*rotation_vector[2])) * 100
                position_camera = -np.matrix(R).T * np.matrix(tvec)
                cos_beta = math.sqrt(R[2,1] * R[2,1] + R[2,2] * R[2,2])
                validity = cos_beta < 1e-6
                if not validity:
                    alpha = math.atan2(R[1,0], R[0,0])    # yaw   [z]
                    beta  = math.atan2(-R[2,0], cos_beta) # pitch [y]
                    gamma = math.atan2(R[2,1], R[2,2])    # roll  [x]
                else:
                    alpha = math.atan2(R[1,0], R[0,0])    # yaw   [z]
                    beta  = math.atan2(-R[2,0], cos_beta) # pitch [y]
                    gamma = 0      
                            #cv2.imwrite(str(rotation_value)+'.jpeg',image)
                euler = np.array([alpha, beta, gamma])
                            #angles = np.zeros(shape = (1,3))
                angles = list()
                for x in euler:
                    angles.append(math.degrees(x))

                            
                yaw = angles[0]
                pitch = angles[1]
                roll = angles[2]
                #print(int(yaw),int(pitch))
                if int(yaw) in yaw_r and int(pitch) in pitch_r:
                    time_spent.append(datetime.now())
                    #print("here")
                    
            if len(faces_array) < previous_imp and len(time_spent) > 0:
                n = time_spent[0]
                t = time_spent[len(time_spent) - 1]
                d = t - n
                impr = impr+1
                print(str(datetime.now())+"impression "+ str(impr))
                print(str(datetime.now())+"time "+str(d.seconds))
                t = Process(target = insert_dynamo_impressions, args = (impr,d.seconds,))
                t.start()
                t.join()
                time_spent = []
            #if len(links_list) >= 1:
                #print("face detected, get_results initilized")
                #th = Process(target = get_results, args = (links_list,))
                #th.start()
                #th.join()
                #links_list = []
                #previous_count = previous_count+len(names)
            if len(faces_array) > 0:
                results = model.predict(faces)
                predicted_genders = results[0]
                ages = np.arange(0, 101).reshape(101, 1)
                predicted_ages = results[1].dot(ages).flatten()
                for i, d in enumerate(faces_array):
                    ages_data.append(int(predicted_ages[i]))
                    gender_data.append("female" if predicted_genders[i][0] > 0.5 else "male")
                
                    #label = "{}, {}".format(int(predicted_ages[i]),
                    #            "F" if predicted_genders[i][0] > 0.5 else "M")
                    #draw_label(frame, (d[0], d[1]), label)
                #print(predicted_ages)
                #print("F" if predicted_genders[0] > 0.5 else "M")
            if diff.seconds == 3600:
                print("inserting count")
                Thread( target = insert_dynamo_count, args= (counter,)).start()
                counter = 0
                now = datetime.now()
            
                
            #if len(faces_array) > previous_imp:
            #    dif = len(faces_array) - previous_imp
            #    impressions = impressions+dif
            #    print(str(datetime.now())+"Count "+str(impressions))
            #    t = Process(target = insert_dynamo_local, args = (ages_data,gender_data,))
            #    t.start()
            #    t.join()
            #    impressions = 0
            #    print(ages_data)
            #    print(gender_data)
                
            previous_imp = len(faces_array)
            #cv2.imshow('frame', frame)
            #if cv2.waitKey(1) & 0xFF == ord('q'):break
    vs.stop()
    cv2.destroyAllWindows()



def main_webcam():
    
    #Defining the video capture object
    

    #Create the main window and move it
    #cv2.namedWindow('Video')
    #cv2.moveWindow('Video', 20, 20)

    #Obtaining the CAM dimension
   

    #Defining the camera matrix.
    #To have better result it is necessary to find the focal
    # lenght of the camera. fx/fy are the focal lengths (in pixels) 
    # and cx/cy are the optical centres. These values can be obtained 
    # roughly by approximation, for example in a 640x480 camera:
    # cx = 640/2 = 320
    # cy = 480/2 = 240
    # fx = fy = cx/tan(60/2 * pi / 180) = 554.26
    

    #print("Estimated camera matrix: \n" + str(camera_matrix) + "\n")

    #These are the camera matrix values estimated on my webcam with
    # the calibration code (see: src/calibration):
    #camera_matrix = numpy.float32([[602.10618226,          0.0, 320.27333589],
     #                              [         0.0, 603.55869786,  229.7537026], 
     #                              [         0.0,          0.0,          1.0] ])
    
    #vs = manager.list()
    frameSize = (640, 480)
    vs = WebcamVideoStream(src=0).start()
    time.sleep(0.1)
    cam_w = 640
    cam_h = 480
    c_x = cam_w / 2
    c_y = cam_h / 2
    f_x = c_x / numpy.tan(60/2 * numpy.pi / 180)
    f_y = f_x

    #Estimated camera matrix values.
    camera_matrix = numpy.float32([[f_x, 0.0, c_x],
                                   [0.0, f_y, c_y], 
                                   [0.0, 0.0, 1.0] ])

    print(str(datetime.now())+"Estimated camera matrix: \n" + str(camera_matrix) + "\n")
    #Distortion coefficients
    #camera_distortion = numpy.float32([0.0, 0.0, 0.0, 0.0, 0.0])

    #Distortion coefficients estimated by calibration
    camera_distortion = numpy.float32([ 0.06232237, -0.41559805,  0.00125389, -0.00402566,  0.04879263])


    #This matrix contains the 3D points of the
    # 11 landmarks we want to find. It has been
    # obtained from antrophometric measurement
    # on the human head.
    landmarks_3D = numpy.float32([P3D_RIGHT_SIDE,
                                  P3D_GONION_RIGHT,
                                  P3D_MENTON,
                                  P3D_GONION_LEFT,
                                  P3D_LEFT_SIDE,
                                  P3D_FRONTAL_BREADTH_RIGHT,
                                  P3D_FRONTAL_BREADTH_LEFT,
                                  P3D_SELLION,
                                  P3D_NOSE,
                                  P3D_SUB_NOSE,
                                  P3D_RIGHT_EYE,
                                  P3D_RIGHT_TEAR,
                                  P3D_LEFT_TEAR,
                                  P3D_LEFT_EYE,
                                  P3D_STOMION])

    #Declaring the two classifiers
    #my_cascade = haarCascade("../..//etc/xml/haarcascade_frontalface_alt.xml", "../../etc/xml/haarcascade_profileface.xml")
    hfd = HaarFaceDetector("./etc/xml/haarcascade_frontalface_alt.xml", "./etc/xml/haarcascade_profileface.xml")
    
    #TODO If missing, example file can be retrieved from http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2
    my_detector = faceLandmarkDetection('./etc/shape_predictor_68_face_landmarks.dat')
    #my_face_detector = dlib.get_frontal_face_detector()
    #Error counter definition
    no_face_counter = 0

    #Variables that identify the face
    #position in the main frame.
    face_x1 = 0
    face_y1 = 0
    face_x2 = 0
    face_y2 = 0
    face_w = 0
    face_h = 0

    #Variables that identify the ROI
    #position in the main frame.
    DEBUG=True
    tracker = dlib.correlation_tracker()
    trackingFace = 0
   
    face_n = list()
    count_person = list()
    yaw_r = range(-112,150)
    pitch_r= range(0,90)
    df = pd.DataFrame()
    impressions = 0
    previous_imp = 0
    impr = 0
    time_spent = []
    name1 = './emotion_images/'+"person_"+str(datetime.now())+".jpg"
    boxes = []
    previous_count = 0
    counter = 0
    previous_enc = []
    links_list = []
    box2 = []
    now  = datetime.now()
    check = True
    timer = []
    global lock
    global now_2
    while(True):
        then = datetime.now()
        diff = then - now
        #video_capture = cv2.VideoCapture("rtsp://192.168.1.104")
        #frames = video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
        #print(frames)
        #video_capture.set(cv2.CAP_PROP_POS_FRAMES,frames-1)
        

        
            #count_f = count_f.put([f])
        
        #if inv == False:
            
        #    print(f)
            
        #Create the main window and move it
        #cv2.namedWindow('Video')
        #cv2.moveWindow('Video', 20, 20)
        # Capture frame-by-frame
        frame = vs.read()
        if frame is not None:
            #lock.acquire()
            #frame = input_q.get()
            #lock.release()
            #frame = imutils.resize(frame, width=500)
            #frame = cv2.cvtColor(frame[roi_y1:roi_y2, roi_x1:roi_x2], cv2.COLOR_BGR2GRAY)

            #Looking for faces with cascade
            #The classifier moves over the ROI
            #starting from a minimum dimension and augmentig
            #slightly based on the scale factor parameter.
            #The scale factor for the frontal face is 1.10 (10%)
            #Scale factor: 1.15=15%,1.25=25% ...ecc
            #Higher scale factors means faster classification
            #but lower accuracy.
            #
            #Return code: 1=Frontal, 2=FrontRotLeft, 
            # 3=FrontRotRight, 4=ProfileLeft, 5=ProfileRight.
            #my_cascade.findFace(gray, True, True, True, True, 1.10, 1.10, 1.15, 1.15, 40, 40, rotationAngleCCW=30, rotationAngleCW=-30, lastFaceType=my_cascade.face_type)
            faces_array = hfd.returnMultipleFacesPosition(frame, runFrontal=True, runFrontalRotated=False, 
                    runLeft=False, runRight=False, 
                    frontalScaleFactor=1.2, rotatedFrontalScaleFactor=1.2, 
                    leftScaleFactor=1.15, rightScaleFactor=1.15, 
                    minSizeX=64, minSizeY=64, 
                    rotationAngleCCW=30, rotationAngleCW=-30)
            #print("Total Faces: " + str(len(faces_array)))
            for element in faces_array:

                face_x1 = int(element[0])
                face_y1 = int(element[1])
                face_x2 = int(face_x1+element[2])
                face_y2 = int(face_y1+element[3])
                text_x1 = face_x1
                text_y1 = face_y1 - 3
                #roi_x1 = face_x1 - roi_resize_w
                face_n = [face_x1,face_y1,face_x2,face_y2]
                links_list.append(cropFace(face_n,frame,name1))
                
                

                if DEBUG == True:
                    cv2.putText(frame, "FACE " + str(1), (text_x1,text_y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1);
                    cv2.rectangle(frame, 
                                 (face_x1, face_y1), 
                                 (face_x2, face_y2), 
                                 (0, 255, 0), 
                                  2)            

                landmarks_2D = my_detector.returnLandmarks(frame, face_x1, face_y1, face_x2, face_y2, points_to_return=TRACKED_POINTS)

                if DEBUG == True:
                    for point in landmarks_2D:
                        cv2.circle(frame,( point[0], point[1] ), 2, (0,0,255), -1)

                retval, rvec, tvec = cv2.solvePnP(landmarks_3D, 
                                                      landmarks_2D, 
                                                      camera_matrix, camera_distortion)

               
                axis = numpy.float32([[50,0,0], 
                                          [0,50,0], 
                                          [0,0,50]])
                
                R = cv2.Rodrigues(rvec)[0]
                            #rotation_value =  math.sqrt(abs(rotation_vector[0]*rotation_vector[0]) + abs(rotation_vector[1]*rotation_vector[0]) + abs(rotation_vector[2]*rotation_vector[2])) * 100
                position_camera = -np.matrix(R).T * np.matrix(tvec)
                cos_beta = math.sqrt(R[2,1] * R[2,1] + R[2,2] * R[2,2])
                validity = cos_beta < 1e-6
                if not validity:
                    alpha = math.atan2(R[1,0], R[0,0])    # yaw   [z]
                    beta  = math.atan2(-R[2,0], cos_beta) # pitch [y]
                    gamma = math.atan2(R[2,1], R[2,2])    # roll  [x]
                else:
                    alpha = math.atan2(R[1,0], R[0,0])    # yaw   [z]
                    beta  = math.atan2(-R[2,0], cos_beta) # pitch [y]
                    gamma = 0      
                            #cv2.imwrite(str(rotation_value)+'.jpeg',image)
                euler = np.array([alpha, beta, gamma])
                            #angles = np.zeros(shape = (1,3))
                angles = list()
                for x in euler:
                    angles.append(math.degrees(x))

                            
                yaw = angles[0]
                pitch = angles[1]
                roll = angles[2]
                #print(int(yaw),int(pitch))
                if int(yaw) in yaw_r and int(pitch) in pitch_r:
                    time_spent.append(datetime.now())
                    #print("here")
                    
            if len(faces_array) < previous_imp and len(time_spent) > 0:
                n = time_spent[0]
                t = time_spent[len(time_spent) - 1]
                d = t - n
                impr = impr+1
                print(str(datetime.now())+"impression "+ str(impr))
                print("time "+str(d.seconds))
                time_spent = []
            if len(links_list) >= 1:
                print(str(datetime.now())+"face detected, get_results initilized")
                th = Process(target = get_results2, args = (links_list,))
                th.start()
                th.join()
                links_list = []
                #previous_count = previous_count+len(names)
                
            if diff.seconds == 3600:
                print(str(datetime.now())+"inserting count")
                Thread( target = insert_dynamo_count2, args= (counter,)).start()
                counter = 0
                now = datetime.now()
                    
                
            if len(faces_array) > previous_imp:
                dif = len(faces_array) - previous_imp
                impressions = impressions+dif
                print(str(datetime.now())+"Count "+str(impressions))
            previous_imp = len(faces_array)
            #cv2.imshow('frame', frame)
            #if cv2.waitKey(1) & 0xFF == ord('q'):break
    vs.stop()
    cv2.destroyAllWindows()








if __name__ == "__main__":

    #input_q = Queue(maxsize=50)
    input_q1 = Queue(maxsize=100)
    input_q = Queue(maxsize=100)
    output_q = Queue(maxsize=50)
    results_q = Queue(maxsize=50)
    loc_q = Queue(maxsize=1)
    
    #now = datetime.now()
    #now_2 = datetime.now()
    #connection = "rtsp://192.168.1.4"
    #tcp_server = Thread(target=tcpConnection, args=())
    #tcp_server.start()

    #stream = Process(target = start_stream, args=(input_q,)).start()
    
    
    #pool = Pool(1,main,())

    
    
    #run_engage = Process(target = main_pi,args=())
    #p.daemon = True
    #p.start()
    #run_engage.daemon = True
    #run_engage.start()
    #start_ad_playing.daemon = True

    #display = Process(target= display, args=(output_q,))
    #display.daemon = True
    #display.start()
    #pool.start()
    

    #insert_dynamo_local([23,24],['female','male'])
    
    #p.join()
    main_webcam()
    #run_engage.join()
    #get_f.join()

    #tcp_server.terminate()

    #head_pose = Process(target=main, args=(stream,input_q1,output_q,cam_w,cam_h,camera_matrix,results_q))
    #head_pose.start()

    


    





