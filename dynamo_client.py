import boto3
import time
from datetime import datetime
from boto3.dynamodb.conditions import Key, Attr
session = boto3.session.Session()
dynamodb = session.resource('dynamodb')
tableName = 'advertelligent_engagement'
def table_name():
	global tableName
	table = dynamodb.create_table(
	    TableName=tableName,
	    KeySchema=[
	        {
	            'AttributeName': 'location_id',
	            'KeyType': 'HASH'
	        },
	        {
	            'AttributeName': 'timestamp',
	            'KeyType': 'RANGE'
	        }
	    ],
	    AttributeDefinitions=[
	        {
	            'AttributeName': 'location_id',
	            'AttributeType': 'N'
	        },
	        {
	            'AttributeName': 'timestamp',
	            'AttributeType': 'S'
	        }

	    ],
	    ProvisionedThroughput={
	        'ReadCapacityUnits': 5,
	        'WriteCapacityUnits': 5
	    }
	)
	table.meta.client.get_waiter('table_exists').wait(TableName=tableName)
	print(tableName.item_count)


def insert_item(table_name, item):
        """Insert an item to table"""
        #dynamodb = conn
        table = dynamodb.Table(table_name)
        response = table.put_item(Item=item)
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
        	return False

def get_item(table_name, query_item):
        """
        Get an item given its key
        """
        #dynamodb = self.conn
        table = dynamodb.Table(table_name)
        response = table.get_item(
            Key=query_item
        )
        item = response['Item']
        return item


#table_name()
def insert_1():
	absolute_junk = {
	    "ad_id": 34,
	    "ad_url": "videos/3bHFbglWuSKpUBJw9USQlKxyJ17LzNPJy55d3qVe.mp4",
	    "person 0": {"gender": "male","age":int(round(float(43.5+1))),"engagement_score":96,"emotion_average":10},
	    "person 1": {"gender": "male","age":int(round(float(25.5+1))),"engagement_score":76,"emotion_average":8},
	}

	
	for example_counter in range(10):
		required_hash_data = {
			"location_id": 101,
			"timestamp": str(datetime.now())
			}
		item = dict(required_hash_data, **absolute_junk)
		print(insert_item(tableName,item))
		time.sleep(0.1)

insert_1()
table1 = dynamodb.Table(tableName)
response = table1.query(
    KeyConditionExpression=Key('location_id').eq(101)
)

#for i in response['Items']:
#    print(i['ad_id'], ":", i['engagement_score'], ":",i['timestamp'],":",i['device_id'],":",i['ad_url'],":",i['gender'])

