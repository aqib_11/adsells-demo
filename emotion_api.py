#import httplib, urllib, base64
import json
import requests
from uploadvideo import *
import csv
emotion_score = list()
def get_emotion(url1):
    myfile5 = open('./emotion.csv','a')
    t3 = csv.writer(myfile5, delimiter=',',lineterminator='\n')
    t3.writerow(["picture-url","emotion","value"])
    headers = {
        # Request headers
        'Content-Type': 'application/json',
        
        # NOTE: Replace the "Ocp-Apim-Subscription-Key" value with a valid subscription key.
        'Ocp-Apim-Subscription-Key': '611a0b762f9a441aa9fd113a659c239f',
    }
    face_api_url = 'https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect'
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,emotion',
    }

    comp_gender = list()
    comp_emot = list()
    comp_age = list()
    for u in url1:
        url = uploadToS3(u)
        url = "https://s3.ap-south-1.amazonaws.com/face-api-temp-storage/"+url
        print(url)
        response = requests.post(face_api_url, params=params, headers=headers, json={"url": url})
        faces = response.json()
        if faces:
            print(faces)

            comp_emot.append((faces[0]['faceAttributes']['emotion']))
            comp_gender.append(faces[0]['faceAttributes']['gender'])
            comp_age.append(faces[0]['faceAttributes']['age'])
            emotion = (faces[0]['faceAttributes']['emotion'])
        #for key,value in emotion.items():
        #    t3.writerow([url,key,value])
    return comp_emot,comp_gender,comp_age
    
    
    #return emotion_score

#data = get_facial_features(url)
#print(data)
#url1 = ['https://s3.ap-south-1.amazonaws.com/faceapi-temp-storage/Kw4b3mxnKH.jpeg','https://s3.ap-south-1.amazonaws.com/faceapi-temp-storage/zTg8ReiiU6.jpeg','https://s3.ap-south-1.amazonaws.com/faceapi-temp-storage/ntb8fdn1jf.jpeg']
#get_emotion(url1)
def calc_emotion(comp_emot):
    happy = list()
    neutral = list()
    emotion_score_1 = list()
    for x in comp_emot:
        for key,value in x.items():
            if key=='happiness':
                print(key)
                print(value)
                happy.append(value)
            elif key=='neutral':
                print(key)
                print(value)
                neutral.append(value)
            
    happy = [int(x*10) for x in happy]
    print(len(happy))
    print(len(neutral))
    neutral = [int(y*10) for y in neutral]
    for d,i in enumerate(happy):
        print(i)
        if i in range(7,11):
            emotion_score_1.append(10)
        elif i in range(0,4):
            emotion_score_1.append(2)
        elif i in range(4,7):
            emotion_score_1.append(6)
        
    print(emotion_score_1)
    return emotion_score_1
