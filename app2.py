import cv2
import numpy as np
import numpy
import time
from awss3 import *

import json
from microsoftfaceapi3 import *
from pytz import timezone



import os

import re
import sys
from deepgaze.face_detection import HaarFaceDetector
from deepgaze.face_landmark_detection import faceLandmarkDetection
import math
import csv
import imutils

from threading import Thread
import json
from datetime import datetime
#from imutils.video import WebcamVideoStream
import boto3
from boto3.dynamodb.conditions import Key, Attr
from app_utils import *

import pandas as pd
import dlib
from imutils.video import VideoStream
from imutils import face_utils




display = list()
ACCESS_KEY_ID = 'AKIAJIXYW6OXOXAPWIMQ'
ACCESS_SECRET_KEY = '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'


#If True enables the verbose mode
DEBUG = False



P3D_RIGHT_SIDE = numpy.float32([-100.0, -77.5, -5.0]) #0
P3D_GONION_RIGHT = numpy.float32([-110.0, -77.5, -85.0]) #4
P3D_MENTON = numpy.float32([0.0, 0.0, -122.7]) #8
P3D_GONION_LEFT = numpy.float32([-110.0, 77.5, -85.0]) #12
P3D_LEFT_SIDE = numpy.float32([-100.0, 77.5, -5.0]) #16
P3D_FRONTAL_BREADTH_RIGHT = numpy.float32([-20.0, -56.1, 10.0]) #17
P3D_FRONTAL_BREADTH_LEFT = numpy.float32([-20.0, 56.1, 10.0]) #26
P3D_SELLION = numpy.float32([0.0, 0.0, 0.0]) #27
P3D_NOSE = numpy.float32([21.1, 0.0, -48.0]) #30
P3D_SUB_NOSE = numpy.float32([5.0, 0.0, -52.0]) #33
P3D_RIGHT_EYE = numpy.float32([-20.0, -65.5,-5.0]) #36
P3D_RIGHT_TEAR = numpy.float32([-10.0, -40.5,-5.0]) #39
P3D_LEFT_TEAR = numpy.float32([-10.0, 40.5,-5.0]) #42
P3D_LEFT_EYE = numpy.float32([-20.0, 65.5,-5.0]) #45
#P3D_LIP_RIGHT = numpy.float32([-20.0, 65.5,-5.0]) #48
#P3D_LIP_LEFT = numpy.float32([-20.0, 65.5,-5.0]) #54
P3D_STOMION = numpy.float32([10.0, 0.0, -75.0]) #62

#The points to track
#These points are the ones used by PnP
# to estimate the 3D pose of the face
TRACKED_POINTS = (0, 4, 8, 12, 16, 17, 26, 27, 30, 33, 36, 39, 42, 45, 62)
ALL_POINTS = list(range(0,68)) #Used for debug only



data = list()

people_data = list()
data = list()
face_ids = list()
face_ids2 = list()

emotion_images = list()

#lock = multiprocessing.Lock()


def start_stream(input_q):
    global lock
    usingPiCamera = True
    #vs = manager.list()
    frameSize = (640, 480)
    vs = VideoStream(src=0, usePiCamera=usingPiCamera, resolution=frameSize,
		    framerate=10).start()
    time.sleep(0.1)
    global lock
    while 1:
        frame = vs.read()
        if frame is not None:
            #lock.acquire()
            #if input_q1.qsize() <= 5:
                #input_q1.put(frame)
            #if input_q1.qsize() > 40:
                #lock.acquire()
                #while input_q1.qsize() != 20:
                #    input_q1.get()
                #lock.release()
           
            #input_q1.put(frame)
           
            if input_q.qsize() < 20:
                input_q.put(frame)
            else:
                lock.acquire()
                while input_q.qsize() != 0:
                    input_q.get()
                lock.release()
                input_q.put(frame)
            #lock.release()




def cropFace(faces,frame,name):
    name = "./emotion_images/"+name+".jpg"
    image = frame[faces[1] -20:faces[3]+20, faces[0] -20 :faces[2]+20]
    image = imutils.resize(image, width=400)
    if cv2.imwrite(name,image):
        return name
    else:
        return None

def insert_item(table_name, item):
    session = boto3.session.Session()
    dynamodb = session.resource('dynamodb', 
        aws_access_key_id=ACCESS_KEY_ID,
        aws_secret_access_key=ACCESS_SECRET_KEY,
        region_name='us-east-2')
    table = dynamodb.Table(table_name)
    response = table.put_item(Item=item)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return True
    else:
        return False


    #return insert




def insert_dynamo(df,tim):

    tableName = 'demo'

    person = df['person']
    score = df['averages']
    #for i,u in enumerate(url):
    if len(person) > 1:
        for x in range(len(person)):
            eng = score[x]*100
            #print(eng)
            face_data = {"engagement_score":int(eng), "device": "webcam","time":tim}
            #final_emotion.append(emotion_data)
            #final_emotion = dict(final_emotion, **emotion_data)

            #face_parameters = {"ad_url": url,"ad_id":ad_id}
        #engagement_parameters = dict(engagement_parameters, **final_emotion)


            required_hash_data = {
                "gender": "engagament",
                "timestamp": str(datetime.now(timezone('Asia/Karachi')))
                }
            item = dict(required_hash_data, **face_data)
            insert = insert_item(tableName,item)
            print(insert)
    else:
        face_data = {"engagement_score":int(score[0]), "device": "webcam"}
        


        required_hash_data = {
            "gender": "engagement",
            "timestamp": str(datetime.now(timezone('Asia/Karachi')))
            }
        item = dict(required_hash_data, **face_data)
        insert = insert_item(tableName,item)
        #print(insert)

    #return insert
    
    
    

    

def insert_dynamo2(links,faces_data,genders,ages,score,times):

    tableName = 'demo'
    links = [x for x in links if x is not None]
    faces_data = [y for y in faces_data if y is not None]
    genders = [a for a in genders if a is not None]
    ages = [b for b in ages if b is not None]

    #for i,u in enumerate(url):
    #print("Engagement Score:==> "+str(score))
    for i,url in enumerate(links):
        #if genders[0] is not None:
        score = score[i]*100
        eng = int(round(float(score)))
        face_data = {"url":url,"age":int(round(float(ages[i]))),"faces_data":str(faces_data[i]),"engagement_score":eng,"time":times[i]}
        #final_emotion.append(emotion_data)
        #final_emotion = dict(final_emotion, **emotion_data)

        #face_parameters = {"ad_url": url,"ad_id":ad_id}
    #engagement_parameters = dict(engagement_parameters, **final_emotion)


        required_hash_data = {
            "gender": genders[i],
            "timestamp": str(datetime.now(timezone('Asia/Karachi')))
            }
        item = dict(required_hash_data, **face_data)
        insert = insert_item(tableName,item)
        if insert:
            print("Results successfuly sent to CMS")
            print("-------------------------------------------------------------------------------------------")

        else:
            print("Error in sending results to CMS - Please check your internet connection")
        #print(insert)
        #return insert



def get_results(name):
    global face_ids
    ages = []
    genders = []
    links = []
    datas = []
    for img_path in name:
        imagename = uploadToS3(img_path)
        name1 = 'https://s3.ap-south-1.amazonaws.com/face-api-temp-storage/'+imagename
        response1 = get_facial_features(name1)
        response = response1.decode("utf-8")
        if response:
            data = json.loads(str(response))
            data2 = data
            if data:
        
                #print ("Response")
                
                #print (response)
                
                gender = str(data[0]['faceAttributes']['gender'])
                age1 = str(data[0]['faceAttributes']['age'])
                beard = str(data[0]['faceAttributes']['facialHair']['beard'])
                

                age1 = float(age1) - (10*(float(beard)) + 5)
                
                face_ids.append(data[0]['faceId'])
                                        
                if(len(face_ids) >= 2):
                    
                    if (is_same_person(face_ids[0], face_ids[1])):
                        print("Same Person infront of the camera")
                        print("-------------------------------------------------------------------------------------------")
                        face_ids.pop(0)
                        ages.append(None)
                        genders.append(None)
                        links.append(None)
                        datas.append(None)
                        continue
                    else:
                        print("Gender:==> "+gender)
                        print("Age:==> "+str(age1))
                        
                    face_ids.pop(0)
                else:
                    print("Gender:==> "+gender)
                    print("Age:==> "+str(age1))
                    ages.append(age1)
                    genders.append(gender)
                    links.append(name1)
                    datas.append(data2)
        else:
            print('no data from API')
    return ages,genders,links,datas
            


def score(df,links_list,times):


    person1 = df['person']
    final_data = list()
    person1 = list(person1)
    #print("person->",person1)
    #a = 2
    if len(links_list) >= 1:
        print("Predicting age and gender...")
        age,gender,links,datas = get_results(links_list)
    if len(person1) > 0:
        for i in range(max(person1)+1):
            results = list()
            person_data = df[df.person == i]
            #print(person_data)
            #timed = person_data['timediff']
            results = person_data['weights']

            #for t in range(max(timed)):
            #    if t % a == 0:
            #        r = person_data.weights.where(person_data.timediff <= a)
            #        results.append(list(r))
            #        person_data= pd.concat([person_data,r]).drop_duplicates(keep=False)
            #        a = a+2
            #    else:
                    
            results = list(results)
            #results = [x for x in results if str(x)!='nan']
            #df2 = pd.DataFrame(columns=['person','averages'])
            
           
            #print(results)
            if results:
                b = sum(results)/len(results)
                results_avg = b


                print("Engagement Score:==> ",str(results_avg*100))
                #emot,gender,emotion_score = emotion_calc(emotion_score)
               # t5.writerow([person,position,distance,z,loc,url,viewers,per_1+per_65,per_1,per_65,per_0,avg_eng_2,per_1_2,per_65_2,n_avg_eng_2,no_avg,norm_avg,average_2,average_22,normal_2,weighted_avg,weighted_avg22,n_weighted_avg,normal,n_normal,norm_avg-no_avg,n_weighted_avg-weighted_avg,n_normal-normal])
                #myfile5.flush()
                
                final_data.append(results_avg)
                #print(insert_dynamo(loc,url,z,viewers,results_avg[3]*100))
                    
            else:
                print("No results")
            #except Exception as e:
            #    print("average exception", e)




        #for i,dat in enumerate(final_data):
        #    df2.loc[i] = dat
        Thread(target = insert_dynamo2 , args= (links,datas,gender,age,final_data,times,)).start()
        #print("emotion calculation begin")
    else:
        print("No data")








def main_webcam():
    
    #Defining the video capture object
    

    #Create the main window and move it
    #cv2.namedWindow('Video')
    #cv2.moveWindow('Video', 20, 20)

    #Obtaining the CAM dimension
   

    #Defining the camera matrix.
    #To have better result it is necessary to find the focal
    # lenght of the camera. fx/fy are the focal lengths (in pixels) 
    # and cx/cy are the optical centres. These values can be obtained 
    # roughly by approximation, for example in a 640x480 camera:
    # cx = 640/2 = 320
    # cy = 480/2 = 240
    # fx = fy = cx/tan(60/2 * pi / 180) = 554.26
    

    #print("Estimated camera matrix: \n" + str(camera_matrix) + "\n")

    #These are the camera matrix values estimated on my webcam with
    # the calibration code (see: src/calibration):
    #camera_matrix = numpy.float32([[602.10618226,          0.0, 320.27333589],
     #                              [         0.0, 603.55869786,  229.7537026], 
     #                              [         0.0,          0.0,          1.0] ])
    
    #vs = manager.list()

    frameSize = (640, 480)
    vs = WebcamVideoStream(0).start()
    #time.sleep(0.1)
    print("Camera sensor enabled")


    
    cam_w,cam_h = vs.width_height() 
    c_x = cam_w / 2
    c_y = cam_h / 2
    f_x = c_x / numpy.tan(60/2 * numpy.pi / 180)
    f_y = f_x

    #Estimated camera matrix values.
    camera_matrix = numpy.float32([[f_x, 0.0, c_x],
                                   [0.0, f_y, c_y], 
                                   [0.0, 0.0, 1.0] ])

    #print(str(datetime.now())+"Estimated camera matrix: \n" + str(camera_matrix) + "\n")
    #Distortion coefficients
    #camera_distortion = numpy.float32([0.0, 0.0, 0.0, 0.0, 0.0])

    #Distortion coefficients estimated by calibration
    camera_distortion = numpy.float32([ 0.06232237, -0.41559805,  0.00125389, -0.00402566,  0.04879263])


    #This matrix contains the 3D points of the
    # 11 landmarks we want to find. It has been
    # obtained from antrophometric measurement
    # on the human head.
    landmarks_3D = numpy.float32([P3D_RIGHT_SIDE,
                                  P3D_GONION_RIGHT,
                                  P3D_MENTON,
                                  P3D_GONION_LEFT,
                                  P3D_LEFT_SIDE,
                                  P3D_FRONTAL_BREADTH_RIGHT,
                                  P3D_FRONTAL_BREADTH_LEFT,
                                  P3D_SELLION,
                                  P3D_NOSE,
                                  P3D_SUB_NOSE,
                                  P3D_RIGHT_EYE,
                                  P3D_RIGHT_TEAR,
                                  P3D_LEFT_TEAR,
                                  P3D_LEFT_EYE,
                                  P3D_STOMION])

    #Declaring the two classifiers
    #my_cascade = haarCascade("../..//etc/xml/haarcascade_frontalface_alt.xml", "../../etc/xml/haarcascade_profileface.xml")
    hfd = HaarFaceDetector("./etc/xml/haarcascade_frontalface_alt.xml", "./etc/xml/haarcascade_profileface.xml")
    #Thread(target = detectFace, args=(vs,hfd,)).start()
    #TODO If missing, example file can be retrieved from http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2
    my_detector = faceLandmarkDetection('./etc/shape_predictor_68_face_landmarks.dat')
    #my_face_detector = dlib.get_frontal_face_detector()
    #Error counter definition
    no_face_counter = 0

    #Variables that identify the face
    #position in the main frame.
    face_x1 = 0
    face_y1 = 0
    face_x2 = 0
    face_y2 = 0
    face_w = 0
    face_h = 0

    #Variables that identify the ROI
    #position in the main frame.
    DEBUG=False
    tracker = dlib.correlation_tracker()
    trackingFace = 0
   
    face_n = list()
    count_person = list()
    yaw_r = range(-170,170)
    pitch_r= range(70,90)
    df = pd.DataFrame()
    impressions = 0
    previous_imp = 0
    impr = 0
    time_spent = []
    #name1 = './emotion_images/'+"person_"+str(datetime.now())+".jpg"
    boxes = []
    previous_count = 0
    counter = 0
    previous_enc = []
    links_list = []
    box2 = []
    now  = datetime.now()
    check = True
    timer = []
    ya = list()
    pi = list()
    global lock
    global now_2
    times = []
    while(True):
        try:
            name1 = randstring()
            then = datetime.now()
            diff = then - now
            #video_capture = cv2.VideoCapture("rtsp://192.168.1.104")
            #frames = video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
            #print(frames)
            #video_capture.set(cv2.CAP_PROP_POS_FRAMES,frames-1)
            

            
                #count_f = count_f.put([f])
            
            #if inv == False:
                
            #    print(f)
                
            #Create the main window and move it
            #cv2.namedWindow('Video')
            #cv2.moveWindow('Video', 20, 20)
            # Capture frame-by-frame
            ret,frame = vs.read()
            #frame.setflags(write=True)
            if frame is None:
                continue


            if frame is not None:
                frame_n = frame
                #cv2.imshow('frame', frame)
                #if cv2.waitKey(1) & 0xFF == ord('q'):break
                #lock.acquire()
                #frame = input_q.get()
                #lock.release()
                #frame = imutils.resize(frame, width=500)
                #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                #Looking for faces with cascade
                #The classifier moves over the ROI
                #starting from a minimum dimension and augmentig
                #slightly based on the scale factor parameter.
                #The scale factor for the frontal face is 1.10 (10%)
                #Scale factor: 1.15=15%,1.25=25% ...ecc
                #Higher scale factors means faster classification
                #but lower accuracy.
                #
                #Return code: 1=Frontal, 2=FrontRotLeft, 
                # 3=FrontRotRight, 4=ProfileLeft, 5=ProfileRight.
                #my_cascade.findFace(gray, True, True, True, True, 1.10, 1.10, 1.15, 1.15, 40, 40, rotationAngleCCW=30, rotationAngleCW=-30, lastFaceType=my_cascade.face_type)
                faces_array = hfd.returnMultipleFacesPosition(frame, runFrontal=True, runFrontalRotated=False, 
                        runLeft=False, runRight=False, 
                        frontalScaleFactor=1.2, rotatedFrontalScaleFactor=1.2, 
                        leftScaleFactor=1.15, rightScaleFactor=1.15, 
                        minSizeX=64, minSizeY=64, 
                        rotationAngleCCW=30, rotationAngleCW=-30)
                #print("Total Faces: " + str(len(faces_array)))
                for i,element in enumerate(faces_array):

                    face_x1 = int(element[0])
                    face_y1 = int(element[1])
                    face_x2 = int(face_x1+element[2])
                    face_y2 = int(face_y1+element[3])
                    text_x1 = face_x1
                    text_y1 = face_y1 - 3
                    #roi_x1 = face_x1 - roi_resize_w
                    face_n = [face_x1,face_y1,face_x2,face_y2]
                    
                    
                    
                    
                        

                    if DEBUG == True:
                        cv2.putText(frame, "FACE " + str(1), (text_x1,text_y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1);
                        cv2.rectangle(frame, 
                                     (face_x1, face_y1), 
                                     (face_x2, face_y2), 
                                     (0, 255, 0), 
                                      2)            

                    landmarks_2D = my_detector.returnLandmarks(frame, face_x1, face_y1, face_x2, face_y2, points_to_return=TRACKED_POINTS)

                    if DEBUG == True:
                        for point in landmarks_2D:
                            cv2.circle(frame,( point[0], point[1] ), 2, (0,0,255), -1)

                    retval, rvec, tvec = cv2.solvePnP(landmarks_3D, 
                                                          landmarks_2D, 
                                                          camera_matrix, camera_distortion)

                   
                    axis = numpy.float32([[50,0,0], 
                                              [0,50,0], 
                                              [0,0,50]])
                    
                    R = cv2.Rodrigues(rvec)[0]
                                #rotation_value =  math.sqrt(abs(rotation_vector[0]*rotation_vector[0]) + abs(rotation_vector[1]*rotation_vector[0]) + abs(rotation_vector[2]*rotation_vector[2])) * 100
                    position_camera = -np.matrix(R).T * np.matrix(tvec)
                    cos_beta = math.sqrt(R[2,1] * R[2,1] + R[2,2] * R[2,2])
                    validity = cos_beta < 1e-6
                    if not validity:
                        alpha = math.atan2(R[1,0], R[0,0])    # yaw   [z]
                        beta  = math.atan2(-R[2,0], cos_beta) # pitch [y]
                        gamma = math.atan2(R[2,1], R[2,2])    # roll  [x]
                    else:
                        alpha = math.atan2(R[1,0], R[0,0])    # yaw   [z]
                        beta  = math.atan2(-R[2,0], cos_beta) # pitch [y]
                        gamma = 0      
                                #cv2.imwrite(str(rotation_value)+'.jpeg',image)
                    euler = np.array([alpha, beta, gamma])
                                #angles = np.zeros(shape = (1,3))
                    angles = list()
                    for x in euler:
                        angles.append(math.degrees(x))

                                
                    yaw = angles[0]
                    pitch = angles[1]
                    roll = angles[2]
                    #print(int(yaw),int(pitch))
                    #if int(yaw) in yaw_r and int(pitch) in pitch_r:
                        
                        #print("here")
                    if len(faces_array) >= 1:
                        #print("here1")
                        
                        count_person.append(i)
                        #print(count_person)
                        ya.append(int(round(float(yaw))))
                        pi.append(int(round(float(pitch))))
                        #print(ya,pi)
                    if len(faces_array) > previous_imp:
                        links_list.append(cropFace(face_n,frame_n,name1))
                        time_spent.append(datetime.now())

                    
                if len(faces_array) < previous_imp and len(time_spent) > 0:
                    #print(count_person)
                    #if len(time_spent) > 1:
                    for i in time_spent:
                        the = datetime.now()
                        difr = the - i
                        times.append(difr.seconds)

                    #n = time_spent[0]
                    #t = time_spent[len(time_spent) - 1]
                    #d = t - n
                    #print(str(datetime.now())+"time "+str(d.seconds))
                    #print(time_spent)
                    #impr = impr+1
                    #print(str(datetime.now())+"impression "+ str(impr))
                    print("time : "+str(times)+"sec")
                    #tim = d.seconds
                    df['person'] = pd.Series(count_person)
                    #print(df['person'])
                    df['yaw'] = pd.Series(ya)
                    df['pitch'] = pd.Series(pi)
                    count_person = []
                    ya = []
                    pi = []
                    df['weights'] = [1 if x in yaw_r and y in pitch_r else 0.01 for x,y in zip(df['yaw'],df['pitch'])]
                    #print(links_list)

                    score_avg = Thread(target=score, args=(df,links_list,times))
                    score_avg.start()
                    score_avg.join()
                    df = pd.DataFrame()
                    times = []
                    time_spent = []
                #if len(links_list) >= 1:
                #    print(str(datetime.now())+"face detected, get_results initilized")
                #    th = Thread(target = get_results2, args = (links_list,))
                #    th.start()
                #    th.join()
                    links_list = []
                    #previous_count = previous_count+len(names)
                    
                if diff.seconds == 3600:
                    print(str(datetime.now())+"inserting count")
                    Thread( target = insert_dynamo_count2, args= (counter,)).start()
                    counter = 0
                    now = datetime.now()
                        
                    
                if len(faces_array) > previous_imp:
                    dif = len(faces_array) - previous_imp
                    impressions = impressions+dif
                    #print(str(datetime.now())+"Count "+str(impressions))
                previous_imp = len(faces_array)
                #links_list = []
                #cv2.imshow('frame', frame)
                #if cv2.waitKey(1) & 0xFF == ord('q'):break
        except:
            #print(str(e))
            break
    vs.stop()
    cv2.destroyAllWindows()








if __name__ == "__main__":

   
    #connection = "rtsp://192.168.1.4"
    #tcp_server = Thread(target=tcpConnection, args=())
    #tcp_server.start()

    #stream = Process(target = start_stream, args=(input_q,)).start()
    
    
    #pool = Pool(1,main,())

    
    
    #run_engage = Process(target = main_pi,args=())
    #p.daemon = True
    #p.start()
    #run_engage.daemon = True
    #run_engage.start()
    #start_ad_playing.daemon = True

    #display = Process(target= display, args=(output_q,))
    #display.daemon = True
    #display.start()
    #pool.start()
    

    #insert_dynamo_local([23,24],['female','male'])
    
    #p.join()
    print("Application started.")
    main_webcam()
    #run_engage.join()
    #get_f.join()

    #tcp_server.terminate()

    #head_pose = Process(target=main, args=(stream,input_q1,output_q,cam_w,cam_h,camera_matrix,results_q))
    #head_pose.start()

    


    





